package at.femoweb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by felix on 12/23/14.
 */
public class Config {
    
    private static HashMap<String, Properties> config;
    private static NameResolver nameResolver = new NameResolver() {
        @Override
        public String getRealName(Object[] objects) {
            String name = "";
            for(Object object : objects) {
                name += object.toString();
            }
            return name;
        }
    };

    public static void init() {
        config = new HashMap<String, Properties>();
    }
    
    public static void load() {
        File dir = new File("config");
        if(!dir.exists()) {
            dir.mkdirs();
        }
        File[] confs = dir.listFiles();
        for(File conf : confs) {
            if(conf.isFile() && conf.canRead()) {
                Properties properties = new Properties();
                try {
                    properties.load(new FileInputStream(conf));
                } catch (IOException e) {
                    System.out.println("Illegal Configuration file in " + conf.getAbsolutePath());
                }
                config.put(conf.getName(), properties);
            }
            if(!conf.canWrite()) {
                System.out.println("Configuration file is not writeable you may have problems saving the application: " + conf.getAbsolutePath());
            }
        }
    }
    
    public static interface NameResolver {
        public abstract String getRealName(Object[] objects);
    }
    
    public static enum FetchType {
        READ_ONLY, WRITEABLE
    }
    
    public static Properties lookup(FetchType type, Object ... objects) {
        if(objects.length == 1 && objects[0] instanceof String) {
            return prepare(type, config.get(objects[0]));
        } else {
            return prepare(type, config.get(nameResolver.getRealName(objects)));
        }
    }
    
    public static void store() {
        File dir = new File("config");
        for(String name : config.keySet()) {
            File out = new File(dir, name);
            try {
                config.get(name).store(new FileOutputStream(out), null);
            } catch (IOException e) {
                System.out.println("Illegal Configuration file in " + out.getAbsolutePath());
            }
        }
    }
    
    public static void create(String name) {
        config.put(name, new Properties());
    }
    
    public static boolean has(String name) {
        return config.containsKey(name);
    }
    
    private static Properties prepare(FetchType type, Properties properties) {
        if(type == FetchType.READ_ONLY) {
            Properties copy = new Properties();
            for(Object key : properties.keySet()) {
                copy.put(key, properties.get(key));
            }
            return copy;
        } else if (type == FetchType.WRITEABLE) {
            return properties;
        } else {
            return properties;
        }
    }
    
}
