package at.femoweb.data;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by felix on 12/25/14.
 */
public class ConnectionWrapper {
    
    private Connection connection;
    private String name;
    
    public ConnectionWrapper(String name, Connection connection) {
        this.connection = connection;
        this.name = name;
    }
    
    public ConnectionMetadata getMetadata () {
        return new ConnectionMetadata(this);
    }

    public Connection getConnection() {
        return connection;
    }

    public String getName() {
        return name;
    }
    
    public Statement createStatement () throws SQLException {
        return connection.createStatement();
    }
}
