package at.femoweb.data;

/**
 * Created by felix on 12/26/14.
 */
public interface DataChangeListener {

    public abstract void updateData();
}
