package at.femoweb.data;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.util.ArrayList;

/**
 * Created by felix on 12/26/14.
 */
public class GenericSwingListModel implements ComboBoxModel {

    private Class<?> modelDef;
    private Class<?>[] classes;
    private int selected;

    private ArrayList<ListDataListener> listeners;

    public GenericSwingListModel(Class<?> modelDef) {
        this.modelDef = modelDef;
        if (modelDef.isAnnotationPresent(SchemeDefinition.class)) {
            this.classes = modelDef.getAnnotation(SchemeDefinition.class).classes();
        }
        this.listeners = new ArrayList<>();
    }

    @Override
    public void setSelectedItem(Object o) {
        for (int i = 0; i < classes.length; i++) {
            if (classes[i] == o) {
                selected = i;
                return;
            }
        }
    }

    @Override
    public Object getSelectedItem() {
        return classes[selected];
    }

    @Override
    public int getSize() {
        return classes.length;
    }

    @Override
    public Object getElementAt(int i) {
        return classes[i];
    }

    @Override
    public void addListDataListener(ListDataListener listDataListener) {
        listeners.add(listDataListener);
    }

    @Override
    public void removeListDataListener(ListDataListener listDataListener) {
        listeners.remove(listDataListener);
    }
}
