package at.femoweb.data;

/**
 * Created by felix on 12/25/14.
 */
public abstract class Query {

    private String table;
    private String[] fields;

    public Query(String table, String... fields) {
        this.fields = fields;
        this.table = table;
    }

    public abstract String compile();

    public String getTable() {
        return table;
    }

    public String[] getFields() {
        return fields;
    }
}

