package at.femoweb.data;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by felix on 12/25/14.
 */
public class Printer {
    
    public static void printObject(Object object) throws IllegalAccessException {
        Class<?> type = object.getClass();
        System.out.println(type.getName());
        for (Field field : type.getDeclaredFields()) {
            if(field.isAnnotationPresent(Printable.class)) {
                field.setAccessible(true);
                System.out.println("\t" + field.getName() + ":");
                printPart(field.get(object));
            } else if (field.isAnnotationPresent(PrintableList.class)) {
                field.setAccessible(true);
                System.out.println("\t" + field.getName() + ":");
                List<?> list = (List<?>) field.get(object);
                for(Object item : list) {
                    printPart(item);
                }
            }
        }
    }
    
    private static void printPart (Object object) throws IllegalAccessException {
        Class<?> type = object.getClass();
        System.out.print("\t\t" + type.getName() + "{");
        Field[] fields = type.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            field.setAccessible(true);
            System.out.print(field.getName() + ":'" + field.get(object) + "'");
            if(i != fields.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("}");
    }
}
