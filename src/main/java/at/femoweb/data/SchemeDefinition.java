package at.femoweb.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 12/25/14.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SchemeDefinition {
    
    public static enum CreateStrategy {
        CREATE, DROP_CREATE
    }

    public static final int NO_VERSION = -1;
    
    public String name();
    public CreateStrategy createStrategy() default CreateStrategy.DROP_CREATE;
    public int version() default NO_VERSION;
    public Class<?>[] classes();
    
    
}
