package at.femoweb.data;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by felix on 12/25/14.
 */
public class ConnectionMetadata {

    private ConnectionWrapper wrapper;
    private DatabaseMetaData metaData;

    public ConnectionMetadata(ConnectionWrapper wrapper) {
        this.wrapper = wrapper;
        try {
            this.metaData = wrapper.getConnection().getMetaData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public DatabaseMetaData getMetaData() {
        return metaData;
    }

    public int getVersion() throws SQLException {
        ResultSet resultSet = null;
        Statement statement = wrapper.createStatement();
        try {
            resultSet = statement.executeQuery("SELECT _meta_value FROM _femoweb_meta WHERE _meta_key = 'version'");
        } catch (SQLException e) {
            e.printStackTrace();
            return SchemeDefinition.NO_VERSION;
        }
        if (!resultSet.first())
            return SchemeDefinition.NO_VERSION;
        return resultSet.getInt(1);
    }
}
