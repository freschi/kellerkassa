package at.femoweb.data;

/**
 * Created by felix on 12/25/14.
 */
public class NotCorrectlyAnnotatedException extends RuntimeException {

    public NotCorrectlyAnnotatedException(String s) {
        super(s);
    }

    public NotCorrectlyAnnotatedException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public NotCorrectlyAnnotatedException(Throwable throwable) {
        super(throwable);
    }

    protected NotCorrectlyAnnotatedException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
