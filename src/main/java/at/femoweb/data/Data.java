package at.femoweb.data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.time.LocalDate;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by felix on 12/25/14.
 */
public class Data {
    
    private static final Logger logger = LogManager.getLogger(Data.class);
    
    private static HashMap<String, ConnectionWrapper> connections;
    
    public static String init(String url, String username, String password, String name) throws SQLException {
        Connection connection = DriverManager.getConnection(url, username, password);
        if(connections == null)
            connections = new HashMap<>();
        String n = name;
        int count = 0;
        while (connections.containsKey(n)) {
            n = name + (count++);
        }
        connections.put(name, new ConnectionWrapper(name, connection));
        return n;
    }
    
    public static <T> List<T> getData(Class<T> type, String connection) throws SQLException, IllegalAccessException, InstantiationException {
        if(!type.isAnnotationPresent(Table.class)) {
            throw new NotCorrectlyAnnotatedException("Class " + type.getName() + " needs to be annotated with " + Table.class.getName() + " to be read from a database");
        }
        if(!connections.containsKey(connection)) {
            throw new RuntimeException("Unknown connection " + connection);
        }
        Table table = type.getAnnotation(Table.class);
        Field[] fields = type.getDeclaredFields();
        {
            ArrayList<Field> fields_ = new ArrayList<>();
            for(Field field : fields) {
                if(field.isAnnotationPresent(at.femoweb.data.Field.class)) {
                    fields_.add(field);
                }
            }
            if(fields_.size() == 0) {
                throw new NotCorrectlyAnnotatedException("At least one field of " + type.getName() + " needs to be annotated with " + at.femoweb.data.Field.class.getName() + " to be read from a database");
            }
            fields = fields_.toArray(fields);
        }
        Query query = createQuery(table, fields);
        logger.info("Executing Query " + query.compile());
        ArrayList<T> list = new ArrayList<>();
        Statement statement = connections.get(connection).createStatement();
        ResultSet resultSet = statement.executeQuery(query.compile());
        if(!resultSet.first()) {
            return list;
        }
        do {
            T item = type.newInstance();
            for(int i = 0; i < fields.length; i++) {
                fields[i].setAccessible(true);
                setField(item, resultSet, i + 1, fields[i]);
            }
            list.add(item);
        } while (resultSet.next());
        return list;
    }

    private static void setField(Object object, ResultSet resultSet, int index, Field field) throws SQLException, IllegalAccessException {
        if (field.getType() == int.class) {
            field.set(object, resultSet.getInt(index));
        } else if (field.getType() == short.class) {
            field.set(object, resultSet.getShort(index));
        } else if (field.getType() == long.class) {
            field.set(object, resultSet.getLong(index));
        } else if (field.getType() == byte.class) {
            field.set(object, resultSet.getByte(index));
        } else if (field.getType() == double.class) {
            field.set(object, resultSet.getDouble(index));
        } else if (field.getType() == float.class) {
            field.set(object, resultSet.getFloat(index));
        } else if (field.getType() == String.class) {
            field.set(object, resultSet.getString(index));
        } else {
            field.set(object, resultSet.getObject(index));
        }
    }
    
    protected static Query createQuery(final Table table, Field[] fields) {
        String[] f = new String[fields.length];
        for(int i = 0; i < fields.length; i++) {
            f[i] = fields[i].getAnnotation(at.femoweb.data.Field.class).value();
        }
        return new Query(table.value(), f) {
            
            @Override
            public String compile() {
                String query = "SELECT ";
                for(int i = 0; i < getFields().length - 1; i++) {
                    query += getFields()[i] + ", ";
                }
                query += getFields()[getFields().length - 1];
                query += " FROM " + getTable();
                return query;
            }

        };
    }
    
    public static void connect(String connection, Class<?> definer) throws SQLException {
        if(!checkScheme(connection, definer)) {
            ConnectionWrapper wrapper = connections.get(connection);
            int version = SchemeDefinition.NO_VERSION;
            try {
                version = wrapper.getMetadata().getVersion();
            } catch (SQLException e) {
            }
            if(version == SchemeDefinition.NO_VERSION) {
                createScheme(connection, definer);
            } else {
                updateScheme(connection, definer);
            }
        }
    }
    
    public static boolean checkScheme(String connection, Class<?> definer) throws SQLException {
        if(!definer.isAnnotationPresent(SchemeDefinition.class)) {
            throw new NotCorrectlyAnnotatedException("Class " + definer.getName() + " needs to have " + SchemeDefinition.class.getName() + " to be used as scheme definition");
        }
        if(!connections.containsKey(connection)) {
            throw new RuntimeException("Unknown connection " + connection);
        }
        ConnectionWrapper wrapper = connections.get(connection);
        SchemeDefinition definition = definer.getAnnotation(SchemeDefinition.class);
        logger.info("Checking " + definition.name() + " in Version " + definition.version());
        ResultSet resultSet = wrapper.getMetadata().getMetaData().getCatalogs();
        boolean found = false;
        while (resultSet.next()) {
            if(resultSet.getString(1).equalsIgnoreCase(definition.name())) {
                found = true;
            }
        }
        if(!found)
            return false;
        wrapper.getConnection().setCatalog(definition.name());
        int version = wrapper.getMetadata().getVersion();
        return version == definition.version();
    }
    
    public static void createScheme(String connection, Class<?> definer) throws SQLException {
        if(!definer.isAnnotationPresent(SchemeDefinition.class)) {
            throw new NotCorrectlyAnnotatedException("Class " + definer.getName() + " needs to have " + SchemeDefinition.class.getName() + " to be used as scheme definition");
        }
        if(!connections.containsKey(connection)) {
            throw new RuntimeException("Unknown connection " + connection);
        }
        ConnectionWrapper wrapper = connections.get(connection);
        SchemeDefinition definition = definer.getAnnotation(SchemeDefinition.class);
        logger.info("Creating " + definition.name() + " in Version " + definition.version());
        Class<?>[] classes = definition.classes();
        String queries = "";
        if(definition.createStrategy() == SchemeDefinition.CreateStrategy.DROP_CREATE) {
            queries += "DROP DATABASE IF EXISTS " + definition.name() + ";\n";
            queries += "CREATE DATABASE " + definition.name() + ";\n";
        } else if (definition.createStrategy() == SchemeDefinition.CreateStrategy.CREATE) {
            queries += "CREATE DATABASE IF NOT EXISTS " + definition.name() + ";\n";
        }
        queries += "USE " + definition.name() + ";\n";
        queries += "\n" + createTableDefinitions(classes, definition);
        queries += createForeignKeyDefinitions(classes);
        queries += createVersionInformation(definition);
        logger.info("Executing Script: \n" + queries);
        Statement statement = wrapper.createStatement();
        queries = queries.replace("\n", "");
        for(String query : queries.split(";")) {
            statement.addBatch(query);
        }
        statement.executeBatch();
        logger.info("Execution finished!");
    }
    
    private static String createVersionInformation(SchemeDefinition definition) {
        if(definition.version() != SchemeDefinition.NO_VERSION) {
            return "\nCREATE TABLE _femoweb_meta (_meta_key VARCHAR(50), _meta_value VARCHAR(50));\nINSERT INTO _femoweb_meta (_meta_key, _meta_value) VALUES ('version', '" + definition.version() + "');\n";
        } else {
            return "";
        }
    }
    
    private static String createTableDefinitions(Class<?>[] classes, SchemeDefinition definition) {
        String queries = "";
        for(Class<?> type : classes) {
            if(!type.isAnnotationPresent(Table.class)) {
                continue;
            }
            Table table = type.getAnnotation(Table.class);
            if(definition.createStrategy() == SchemeDefinition.CreateStrategy.DROP_CREATE) {
                queries += "DROP TABLE IF EXISTS " + table.value() + ";\n";
                queries += "CREATE TABLE " + table.value() + " (";
            } else if (definition.createStrategy() == SchemeDefinition.CreateStrategy.CREATE) {
                queries += "CREATE TABLE IF NOT EXISTS " + table.value() + " (";
            }
            Field[] fields = type.getDeclaredFields();
            {
                ArrayList<Field> fields_ = new ArrayList<>();
                for(Field field : fields) {
                    if(field.isAnnotationPresent(at.femoweb.data.Field.class)) {
                        fields_.add(field);
                    }
                }
                if(fields_.size() == 0) {
                    continue;
                }
                fields = fields_.toArray(fields);
            }
            ArrayList<Field> primaryKeys = new ArrayList<>();
            for(int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                if(field.isAnnotationPresent(PrimaryKey.class)) {
                    primaryKeys.add(field);
                }
                queries += field.getAnnotation(at.femoweb.data.Field.class).value() + " " + getType(field.getType());
                queries += ", ";
            }
            queries += "PRIMARY KEY (";
            for (int i = 0; i < primaryKeys.size(); i++) {
                Field field = primaryKeys.get(i);
                queries += field.getAnnotation(at.femoweb.data.Field.class).value();
                if(i != primaryKeys.size() - 1) {
                    queries += ", ";
                }
            }
            queries += "));\n\n";
        }
        return queries;
    }
    
    private static String getType(Class<?> type) {
        if(type == int.class || type == long.class || type == short.class || type == byte.class) {
            return "INTEGER";
        } else if (type == double.class || type == float.class) {
            return "DECIMAL";
        } else if (type == String.class) {
            return "VARCHAR(100)";
        } else if (type == Date.class || type.isAssignableFrom(Calendar.class) || type == LocalDate.class) {
            return "DATE";
        }
        return "BLOB";
    }
    
    private static String createForeignKeyDefinitions(Class<?>[] classes) {
        String queries = "";
        //ArrayList<Field> foreignKeys = new ArrayList<>();
        for(Class<?> type : classes) {
            if(!type.isAnnotationPresent(Table.class)) {
                continue;
            }
            Table table = type.getAnnotation(Table.class);
            for(Field field : type.getDeclaredFields()) {
                if(field.isAnnotationPresent(at.femoweb.data.Field.class) && field.isAnnotationPresent(ForeignKey.class)) {
                    ForeignKey foreignKey = field.getAnnotation(ForeignKey.class);
                    at.femoweb.data.Field item = field.getAnnotation(at.femoweb.data.Field.class);
                    if(!foreignKey.mappedBy().isAnnotationPresent(Table.class)) {
                        throw new NotCorrectlyAnnotatedException("Foreign Key class " + foreignKey.mappedBy().getName() + " must be annotated with " + Table.class.getName());
                    }
                    Table foreignTable = foreignKey.mappedBy().getAnnotation(Table.class);
                    queries += "ALTER TABLE " + table.value() + " ADD FOREIGN KEY (" + item.value() + ") REFERENCES " + foreignTable.value() + "(" + foreignKey.field() + ") ON DELETE CASCADE;\n";
                }
            }
        }
        return queries;
    }
    
    public static void injectData(Object object, String connection) throws IllegalAccessException, SQLException, InstantiationException {
        Class<?> type = object.getClass();
        for(Field field : type.getDeclaredFields()) {
            if(field.isAnnotationPresent(InjectAll.class)) {
                Class<?> dataType = field.getAnnotation(InjectAll.class).value();
                if(dataType.isAnnotationPresent(Table.class)) {
                    List list = getData(dataType, connection);
                    field.setAccessible(true);
                    field.set(object, list);
                }
            }
        }
    }
    
    public static void updateScheme(String connection, Class<?> definer) throws SQLException {
        if(!definer.isAnnotationPresent(SchemeDefinition.class)) {
            throw new NotCorrectlyAnnotatedException("Class " + definer.getName() + " needs to have " + SchemeDefinition.class.getName() + " to be used as scheme definition");
        }
        if(!connections.containsKey(connection)) {
            throw new RuntimeException("Unknown connection " + connection);
        }
        ConnectionWrapper wrapper = connections.get(connection);
        SchemeDefinition definition = definer.getAnnotation(SchemeDefinition.class);
        ArrayList<SchemaUpdate> updates = new ArrayList<>();
        for(Method method : definer.getDeclaredMethods()) {
            if(method.isAnnotationPresent(SchemaUpdate.class)) {
                updates.add(method.getAnnotation(SchemaUpdate.class));
            }
        }
        final int destVersion = definition.version();
        final int currVersion = wrapper.getMetadata().getVersion();
        if(destVersion == currVersion)
            return;;
        logger.info("Updating schema from " + currVersion + " to " + destVersion);
        logger.debug("Looking for suitable direct update");
        if(updates.stream().anyMatch((s) -> s.to() == destVersion && s.from() == currVersion)) {
            SchemaUpdate update = updates.stream().filter((s) -> s.to() == destVersion && s.from() == currVersion).findFirst().get();
            applyUpdates(wrapper, destVersion, update);
        } else {
            ArrayList<SchemaUpdate> schemaUpdates = new ArrayList<>();
            findUpdates(updates, schemaUpdates, currVersion, destVersion);
            applyUpdates(wrapper, destVersion, schemaUpdates.stream().sorted((s1, s2) -> s1.from() - s2.from()).collect(Collectors.toList()).toArray(new SchemaUpdate[]{}));
        }
    }
    
    private static void findUpdates (List<SchemaUpdate> allUpdates, List<SchemaUpdate> updates, int from, int to) {
        if(from == to)
            return;
        List<SchemaUpdate> candidates = allUpdates.stream().filter((s) -> s.to() == to && s.from() >= from).collect(Collectors.toList());
        logger.debug("Found " + candidates.size() + " suitable candidate(s) for " + from + " -> " + to);
        if(candidates.size() == 1) {
            findUpdates(allUpdates, updates, from, candidates.get(0).from());
            updates.add(candidates.get(0));
            return;
        }
        if(candidates.size() == 0) {
            logger.debug("No suitable update found!");
            return;
        }
        SchemaUpdate successor = candidates.get(0);
        int count = Integer.MAX_VALUE;
        for(SchemaUpdate update : candidates) {
            if(update.from() == from && update.to() == to) {
                successor = update;
                break;
            } else {
                ArrayList<SchemaUpdate> schemaUpdates = new ArrayList<>();
                findUpdates(allUpdates, schemaUpdates, update.from(), from);
                if(schemaUpdates.size() < count && schemaUpdates.size() > 0) {
                    logger.debug("Update " + from + " -> " + to + " takes " + schemaUpdates.size() + " steps!");
                    successor = update;
                    count = schemaUpdates.size();
                }
            }
        }
        findUpdates(allUpdates, updates, successor.from(), from);
        updates.add(successor);
    }
    
    private static void applyUpdates (ConnectionWrapper wrapper, int destVersion, SchemaUpdate ... updates) throws SQLException {
        Statement statement = wrapper.createStatement();
        for(SchemaUpdate update : updates) {
            Update[] operations = update.updates();
            logger.debug("Applying update " + update.name() + " (" + update.from() + " -> " + update.to() + ")");
            for(Update operation : operations) {
                Class<?> type = operation.type();
                String name = operation.name();
                Update.UpdateType updateType = operation.updateType();
                if(!type.isAnnotationPresent(Table.class)) {
                    logger.warn("Class " + type.getName() + " needs to be annotated with " + Table.class + " to be accepted as table");
                    continue;
                }
                Table table = type.getAnnotation(Table.class);
                Field field;
                try {
                    field = type.getDeclaredField(name);
                    
                } catch (NoSuchFieldException e) {
                    logger.warn("No field " + name + " in type " + type.getName());
                    continue;
                }
                if(!field.isAnnotationPresent(at.femoweb.data.Field.class)) {
                    logger.warn("Field " + field.getName() + " in type " + type.getName() + " is not annotated with " + at.femoweb.data.Field.class);
                    continue;
                }
                at.femoweb.data.Field info = field.getAnnotation(at.femoweb.data.Field.class);
                switch (updateType) {
                    case ADD_FIELD: {
                        statement.addBatch("ALTER TABLE " + table.value() + " ADD " + info.value() + " " + getType(field.getType()));
                        break;
                    }
                    case ADD_FOREIGN_KEY: {
                        if(!field.isAnnotationPresent(ForeignKey.class)) {
                            logger.warn("Field " + field.getName() + " in type " + type.getName() + " is not annotated with " + ForeignKey.class);
                            continue;
                        }
                        ForeignKey foreignKey = field.getAnnotation(ForeignKey.class);
                        if(!foreignKey.mappedBy().isAnnotationPresent(Table.class)) {
                            logger.warn("Class " + type.getName() + " needs to be annotated with " + Table.class + " to be accepted as table");
                            continue;
                        }
                        Table foreignTable = foreignKey.mappedBy().getAnnotation(Table.class);
                        statement.addBatch("ALTER TABLE " + table.value() + " ADD FOREIGN KEY (" + info.value() + ") REFERENCES " + foreignTable.value() + " (" + foreignKey.field() + ") ON DELETE CASCADE");
                    }
                }
            }
        }
        statement.addBatch("update _femoweb_meta set _meta_value = " + destVersion + " where _meta_key = 'version';");
        statement.executeBatch();
    }
    
    public static void store(String connection, Object ... objects) throws SQLException, IllegalAccessException {
        Statement statement = connections.get(connection).createStatement();
        for(Object object : objects) {
            if(object instanceof Collection) {
                for(Object item : (Collection)object) {
                    store(connection, item);
                }
            } else if (object.getClass().isAnnotationPresent(Table.class)) {
                Class<?> type = object.getClass();
                Table table = type.getAnnotation(Table.class);
                Field[] fields = type.getDeclaredFields();
                {
                    ArrayList<Field> fields_ = new ArrayList<>();
                    for(Field field : fields) {
                        if(field.isAnnotationPresent(at.femoweb.data.Field.class)) {
                            fields_.add(field);
                        }
                    }
                    if(fields_.size() == 0) {
                        continue;
                    }
                    fields = fields_.toArray(fields);
                }
                ArrayList<Field> primaryKeys = new ArrayList<>();
                for(int i = 0; i < fields.length; i++) {
                    Field field = fields[i];
                    if(field.isAnnotationPresent(PrimaryKey.class)) {
                        primaryKeys.add(field);
                    }
                }
                if (checkIfExists(object, table, primaryKeys, connections.get(connection))) {
                    updateObject(object, table, fields, primaryKeys, statement);
                } else {
                    createObject(object, table, fields, primaryKeys, statement);
                }
            }
        }
        statement.executeBatch();
    }

    private static boolean checkIfExists(Object object, Table table, ArrayList<Field> primaryKeys, ConnectionWrapper wrapper) throws SQLException, IllegalAccessException {
        Statement statement = wrapper.createStatement();
        String query = "SELECT COUNT(";
        for (int i = 0; i < primaryKeys.size(); i++) {
            Field field = primaryKeys.get(i);
            at.femoweb.data.Field info = field.getAnnotation(at.femoweb.data.Field.class);
            query += info.value();
            if (i != primaryKeys.size() - 1) {
                query += ", ";
            }
        }
        query += ") FROM " + table.value() + " WHERE ";
        for (int i = 0; i < primaryKeys.size(); i++) {
            Field field = primaryKeys.get(i);
            field.setAccessible(true);
            at.femoweb.data.Field info = field.getAnnotation(at.femoweb.data.Field.class);
            query += info.value() + " = ";
            if (field.getType() == int.class || field.getType() == double.class || field.getType() == float.class || field.getType() == long.class || field.getType() == short.class || field.getType() == byte.class) {
                query += field.get(object);
            } else {
                query += "'" + field.get(object) + "'";
            }
            if (i != primaryKeys.size() - 1) {
                query += " AND ";
            }
        }
        ResultSet resultSet = statement.executeQuery(query);
        return resultSet.first() && resultSet.getInt(1) == 1;
    }

    private static void updateObject(Object object, Table table, Field[] fields, ArrayList<Field> primaryKeys, Statement statement) throws IllegalAccessException, SQLException {
        String query = "UPDATE " + table.value() + " SET ";
        for(int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if(primaryKeys.stream().anyMatch((f) -> f.getName().equalsIgnoreCase(field.getName())))
                continue;
            at.femoweb.data.Field info = field.getAnnotation(at.femoweb.data.Field.class);
            field.setAccessible(true);
            query += info.value() + " = ";
            if(field.getType() == int.class || field.getType() == double.class || field.getType() == float.class || field.getType() == long.class || field.getType() == short.class || field.getType() == byte.class) {
                query += field.get(object);
            } else {
                query += "'" + field.get(object) + "'";
            }
            if(i != fields.length - 1) {
                query += ", ";
            }
        }
        query += " WHERE ";
        for (int i = 0; i < primaryKeys.size(); i++) {
            Field field = primaryKeys.get(i);
            field.setAccessible(true);
            at.femoweb.data.Field info = field.getAnnotation(at.femoweb.data.Field.class);
            query += info.value() + " = ";
            if(field.getType() == int.class || field.getType() == double.class || field.getType() == float.class || field.getType() == long.class || field.getType() == short.class || field.getType() == byte.class) {
                query += field.get(object);
            } else {
                query += "'" + field.get(object) + "'";
            }
            if(i != primaryKeys.size() - 1) {
                query += " AND ";
            }
        }
        statement.addBatch(query);
    }

    private static void createObject(Object object, Table table, Field[] fields, ArrayList<Field> primaryKeys, Statement statement) throws IllegalAccessException, SQLException {
        String query = "INSERT INTO " + table.value() + " SET ";
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            at.femoweb.data.Field info = field.getAnnotation(at.femoweb.data.Field.class);
            field.setAccessible(true);
            query += info.value() + " = ";
            if (field.getType() == int.class || field.getType() == double.class || field.getType() == float.class || field.getType() == long.class || field.getType() == short.class || field.getType() == byte.class) {
                query += field.get(object);
            } else {
                query += "'" + field.get(object) + "'";
            }
            if (i != fields.length - 1) {
                query += ", ";
            }
        }
        statement.addBatch(query);
    }

    public static void remove(Object object, String connection) throws SQLException, IllegalAccessException {
        Class<?> type = object.getClass();
        if(!type.isAnnotationPresent(Table.class)) {
            throw new NotCorrectlyAnnotatedException("Class " + type.getName() + " needs to be annotated with " + Table.class.getName() + " to be read from a database");
        }
        if(!connections.containsKey(connection)) {
            throw new RuntimeException("Unknown connection " + connection);
        }
        Table table = type.getAnnotation(Table.class);
        Field[] fields = type.getDeclaredFields();
        ArrayList<Field> primaryKeys = new ArrayList<>();
        for(int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if(field.isAnnotationPresent(PrimaryKey.class)) {
                primaryKeys.add(field);
            }
        }
        ConnectionWrapper wrapper;
        if(checkIfExists(object, table, primaryKeys, wrapper = connections.get(connection))) {
            String query = "DELETE FROM " + table.value() + " WHERE ";
            for (int i = 0; i < primaryKeys.size(); i++) {
                Field field = primaryKeys.get(i);
                at.femoweb.data.Field info = field.getAnnotation(at.femoweb.data.Field.class);
                field.setAccessible(true);
                query += info.value() + " = ";
                if (field.getType() == int.class || field.getType() == double.class || field.getType() == float.class || field.getType() == long.class || field.getType() == short.class || field.getType() == byte.class) {
                    query += field.get(object);
                } else {
                    query += "'" + field.get(object) + "'";
                }
                if (i != primaryKeys.size() - 1) {
                    query += " AND ";
                }
            }
            Statement statement = wrapper.createStatement();
            statement.execute(query);
            statement.close();
        }
    }
}