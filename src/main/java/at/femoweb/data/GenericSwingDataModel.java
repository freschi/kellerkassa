package at.femoweb.data;

import at.resch.kellerapp.model.Model;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by felix on 12/26/14.
 */
public class GenericSwingDataModel<T> implements TableModel {

    private Class<T> tableDef;
    private Table table;
    private List<T> data;
    private int columns;
    private Field columnNames[];
    private String connection;

    private ArrayList<TableModelListener> listeners;
    private ArrayList<DataChangeListener> dataChangeListeners;

    public GenericSwingDataModel(Class<T> tableDef, String connection) throws IllegalAccessException, SQLException, InstantiationException {
        this.tableDef = tableDef;
        this.table = tableDef.getAnnotation(Table.class);
        data = Data.getData(tableDef, connection);
        ArrayList<Field> names = new ArrayList<>();
        for (Field field : tableDef.getDeclaredFields()) {
            if (field.isAnnotationPresent(at.femoweb.data.Field.class)) {
                field.setAccessible(true);
                names.add(field);
            }
        }
        columnNames = names.toArray(new Field[]{});
        columns = columnNames.length + 1;
        this.connection = connection;
        listeners = new ArrayList<>();
        dataChangeListeners = new ArrayList<>();
    }


    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columns;
    }

    @Override
    public String getColumnName(int i) {
        return i < columns - 1 ? columnNames[i].getName() : "Delete";
    }

    @Override
    public Class<?> getColumnClass(int i) {
        return i < columns - 1 ? String.class : JButton.class;
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return i1 != 0;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        try {
            if (i1 < columns - 1) {
                return columnNames[i1].get(data.get(i)) + "";
            } else {
                JButton button = new JButton("Delete");
                button.addActionListener(event -> {
                    System.out.println("Deleting Object!");
                    Model.getInstance().remove(data.get(i));
                    try {
                        Data.injectData(Model.getInstance(), "db-con");
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    }
                    listeners.forEach(listeners -> listeners.tableChanged(new TableModelEvent(this)));
                    dataChangeListeners.forEach(listener -> listener.updateData());
                });
                return button;
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return "?#!";
    }

    @Override
    public void setValueAt(Object o, int i, int i1) {
        try {
            Field field = columnNames[i1];
            Object item = data.get(i);
            if (field.getType() == int.class) {
                field.set(item, Integer.parseInt(o.toString()));
            } else if (field.getType() == double.class) {
                field.set(item, Double.parseDouble(o.toString()));
            } else {
                field.set(item, o);
            }
            Model.getInstance().add(item);
            data = Data.getData(tableDef, connection);
            listeners.stream().forEach(listener -> listener.tableChanged(new TableModelEvent(this)));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addTableModelListener(TableModelListener tableModelListener) {
        listeners.add(tableModelListener);
    }

    @Override
    public void removeTableModelListener(TableModelListener tableModelListener) {
        listeners.remove(tableModelListener);
    }

    public void addDataChangedListener(DataChangeListener dataChangeListener) {
        dataChangeListeners.add(dataChangeListener);
    }

    public void removeDataChangeListener(DataChangeListener dataChangeListener) {
        dataChangeListeners.remove(dataChangeListener);
    }
}
