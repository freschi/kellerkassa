package at.femoweb.data;

import java.util.List;

/**
 * Created by felix on 12/25/14.
 */
public interface Filter<T> {
    
    public abstract void apply(Class<T> type, List<T> list);
}
