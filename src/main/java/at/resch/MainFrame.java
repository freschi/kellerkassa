package at.resch;

import at.femoweb.data.DataChangeListener;
import at.femoweb.data.GenericSwingDataModel;
import at.femoweb.data.GenericSwingListModel;
import at.femoweb.data.PrimaryKey;
import at.resch.kellerapp.model.Model;
import at.resch.kellerapp.model.ModelDefiner;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EventObject;

/**
 * Created by felix on 12/23/14.
 */
public class MainFrame implements DataChangeListener {
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JTextField textField1;
    private JTable table1;
    private JTextField textField2;
    private JRadioButton eingangRadioButton;
    private JRadioButton ausgangRadioButton;
    private JButton OKButton;
    private JTextField textField3;
    private JTextField textField4;
    private JTable table2;
    private JButton addButton;
    private JList list1;
    private JTextField textField5;
    private JLabel state;
    private JTextField textField6;
    private JTextField textField7;
    private JTextField textField8;
    private JButton addButton1;
    private JTextField textField9;
    private JButton loginButton;
    private JButton addButton2;
    private JComboBox tableSelection;
    private JTable displayTable;
    private JButton addButton4;
    private Model model = Model.getInstance();

    private TableCellEditor buttonEditor = new TableCellEditor() {

        ArrayList<CellEditorListener> listeners = new ArrayList<>();

        @Override
        public Component getTableCellEditorComponent(JTable jTable, Object o, boolean b, int i, int i1) {
            return (JButton) o;
        }

        @Override
        public Object getCellEditorValue() {
            return new JButton("Delete");
        }

        @Override
        public boolean isCellEditable(EventObject eventObject) {
            return true;
        }

        @Override
        public boolean shouldSelectCell(EventObject eventObject) {
            return false;
        }

        @Override
        public boolean stopCellEditing() {
            return true;
        }

        @Override
        public void cancelCellEditing() {
            System.out.println("Canceling!");
        }

        @Override
        public void addCellEditorListener(CellEditorListener cellEditorListener) {
            listeners.add(cellEditorListener);
        }

        @Override
        public void removeCellEditorListener(CellEditorListener cellEditorListener) {
            listeners.remove(cellEditorListener);
        }
    };

    public MainFrame() {
        JFrame frame = new JFrame("MainFrame");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(1000, 800);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        tableSelection.setModel(new GenericSwingListModel(ModelDefiner.class));
        tableSelection.addItemListener(itemEvent -> {
            if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
                try {
                    GenericSwingDataModel model1;
                    displayTable.setModel(model1 = new GenericSwingDataModel<>((Class<Object>) tableSelection.getSelectedItem(), "db-con"));
                    model1.addDataChangedListener(this);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                }
                displayTable.getColumn("Delete").setCellRenderer((jTable, o, b, b1, i, i1) -> (JButton) o);
                displayTable.getColumn("Delete").setCellEditor(buttonEditor);
            }
        });

        try {
            GenericSwingDataModel model1;
            displayTable.setModel(model1 = new GenericSwingDataModel<>((Class<Object>) tableSelection.getSelectedItem(), "db-con"));
            model1.addDataChangedListener(this);
            displayTable.getColumn("Delete").setCellRenderer((jTable, o, b, b1, i, i1) -> (JButton) o);
            displayTable.getColumn("Delete").setCellEditor(buttonEditor);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        addButton4.addActionListener(actionEvent -> {
            try {
                Class<?> type = (Class<?>) tableSelection.getSelectedItem();
                Object n = type.newInstance();
                Field primaryKey = null;
                for (Field field : type.getDeclaredFields()) {
                    if (field.isAnnotationPresent(at.femoweb.data.Field.class) && field.isAnnotationPresent(PrimaryKey.class)) {
                        if (primaryKey == null) {
                            primaryKey = field;
                        } else {
                            return;
                        }
                    }
                }
                primaryKey.setAccessible(true);
                String key = JOptionPane.showInputDialog(null, "Please enter Primary Key for " + type, "PRIMARY KEY", JOptionPane.QUESTION_MESSAGE);
                if (primaryKey.getType() == int.class) {
                    primaryKey.set(n, Integer.parseInt(key));
                } else {
                    primaryKey.set(n, key);
                }
                model.add(n);
                GenericSwingDataModel model1;
                displayTable.setModel(model1 = new GenericSwingDataModel<>((Class<Object>) tableSelection.getSelectedItem(), "db-con"));
                model1.addDataChangedListener(this);
                displayTable.getColumn("Delete").setCellRenderer((jTable, o, b, b1, i, i1) -> (JButton) o);
                displayTable.getColumn("Delete").setCellEditor(buttonEditor);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void updateData() {
        System.out.println("Updating Data");
        try {
            GenericSwingDataModel model;
            displayTable.setModel(model = new GenericSwingDataModel<>((Class<Object>) tableSelection.getSelectedItem(), "db-con"));
            model.addDataChangedListener(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        displayTable.getColumn("Delete").setCellRenderer((jTable, o, b, b1, i, i1) -> (JButton) o);
        displayTable.getColumn("Delete").setCellEditor(buttonEditor);
    }
}
