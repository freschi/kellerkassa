package at.resch;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by felix on 1/3/15.
 */
public class ProductInfoRequest {

    private String barcode;

    public ProductInfoRequest (String barcode) {
        this.barcode = barcode;
    }

    public void execute () {
        try {
            Document document = Jsoup.connect("http://fddb.info/xml/auto/1/de/" + barcode).userAgent("Mozilla/5.0 (X11; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0").ignoreContentType(true).get();
            Elements tr = document.select("tr");
            for(Element element : tr) {
                if(element.attr("class").startsWith("aclist")) {
                    Element link = element.select("td").first();
                    String location = link.attr("onclick");
                    location = location.substring(location.indexOf("'") + 1, location.lastIndexOf("'"));
                    System.out.println(location);
                    Document info = Jsoup.connect(location).userAgent("Mozilla/5.0 (X11; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0").ignoreContentType(true).get();
                    Element name = info.select("h1").first();
                    System.out.println(name.text());
                    for(Element amount : info.select("a.servb")) {
                        System.out.println(amount.text());
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            String barcode = scanner.next();
            ProductInfoRequest request = new ProductInfoRequest(barcode);
            request.execute();
        }
    }
}
