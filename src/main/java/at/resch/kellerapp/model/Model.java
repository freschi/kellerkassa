package at.resch.kellerapp.model;

import at.femoweb.data.Data;
import at.femoweb.data.InjectAll;
import at.femoweb.data.PrintableList;
import at.femoweb.data.Printer;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by felix on 12/25/14.
 */
public class Model {

    public static Model instance;

    @InjectAll(Card.class)
    @PrintableList
    private List<Card> cards;

    @InjectAll(CardType.class)
    @PrintableList
    private List<CardType> cardTypes;

    @InjectAll(Category.class)
    @PrintableList
    private List<Category> categories;

    @InjectAll(Drink.class)
    @PrintableList
    private List<Drink> drinks;

    @InjectAll(Identity.class)
    @PrintableList
    private List<Identity> identities;

    @InjectAll(Mix.class)
    @PrintableList
    private List<Mix> mixes;

    @InjectAll(MoneyResource.class)
    @PrintableList
    private List<MoneyResource> moneyResources;

    @InjectAll(Permission.class)
    @PrintableList
    private List<Permission> permissions;

    @InjectAll(Transaction.class)
    @PrintableList
    private List<Transaction> transactions;

    @InjectAll(User.class)
    @PrintableList
    private List<User> users;

    public static Model getInstance() {
        return instance;
    }

    public List<Card> getCards() {
        return cards;
    }

    public List<CardType> getCardTypes() {
        return cardTypes;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public List<Identity> getIdentities() {
        return identities;
    }

    public List<Mix> getMixes() {
        return mixes;
    }

    public List<MoneyResource> getMoneyResources() {
        return moneyResources;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public List<User> getUsers() {
        return users;
    }

    public void add(Object object) {
        try {
            Data.store("db-con", object);
            Data.injectData(this, "db-con");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    public void remove(Object object) {
        try {
            Data.remove(object, "db-con");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void print() {
        try {
            Printer.printObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
