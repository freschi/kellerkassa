package at.resch.kellerapp.model;

import at.femoweb.data.Field;
import at.femoweb.data.ForeignKey;
import at.femoweb.data.PrimaryKey;
import at.femoweb.data.Table;

/**
 * Created by felix on 8/4/14.
 */
@Table("p_permissions")
public class Permission {

    @Field("p_u_user")
    @PrimaryKey
    @ForeignKey(field = "u_id", mappedBy = User.class)
    private int user;

    @Field("p_permission")
    @PrimaryKey
    private String permission;

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
