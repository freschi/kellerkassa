package at.resch.kellerapp.model;

import at.femoweb.data.SchemaUpdate;
import at.femoweb.data.SchemeDefinition;
import at.femoweb.data.Update;

/**
 * Created by felix on 12/25/14.
 */
@SchemeDefinition(name = "kellerapp", version = 5, classes = {Card.class, CardType.class, Category.class, Drink.class, Identity.class, Ingredient.class, Mix.class, MoneyResource.class, Permission.class, PurchaseUnit.class, Transaction.class, User.class})
public abstract class ModelDefiner {
    
    @SchemaUpdate(from = 1, to = 2, updates = {
            @Update(type = Transaction.class, name = "bill", updateType = Update.UpdateType.ADD_FIELD),
            @Update(type = Transaction.class, name = "comment", updateType = Update.UpdateType.ADD_FIELD)
    })
    public void update_1() {}
    
    @SchemaUpdate(from = 2, to = 3, updates = {
            @Update(type = Card.class, name = "type", updateType = Update.UpdateType.ADD_FOREIGN_KEY)
    })
    public void update_2() {}
    
    @SchemaUpdate(from = 3, to = 4, updates = {}, name = "Test Update")
    public void update_3() {}
    
    @SchemaUpdate(from = 4, to = 5, updates = {})
    public void update_4() {}
}
