package at.resch.kellerapp.model;

import at.femoweb.data.Field;
import at.femoweb.data.PrimaryKey;
import at.femoweb.data.Table;

/**
 * Created by felix on 8/8/14.
 */
@Table("mr_money_resources")
public class MoneyResource {

    @PrimaryKey
    @Field("mr_id")
    private int id;

    @Field("mr_name")
    private String name;

    @Field("mr_name_short")
    private String shortName;

    @Field("mr_amount")
    private double amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
