package at.resch.kellerapp.model;

import at.femoweb.data.Field;
import at.femoweb.data.ForeignKey;
import at.femoweb.data.PrimaryKey;
import at.femoweb.data.Table;

/**
 * Created by felix on 8/8/14.
 */
@Table("t_transactions")
public class Transaction {

    @PrimaryKey
    @Field("t_id")
    private int number;

    @Field("t_mr_from")
    @ForeignKey(field = "mr_id", mappedBy = MoneyResource.class)
    private int from;

    @Field("t_mr_to")
    @ForeignKey(field = "mr_id", mappedBy = MoneyResource.class)
    private int to;

    @Field("t_amount")
    private double amount;
    
    @Field("t_billid")
    private String bill;
    
    @Field("t_comment")
    private String comment;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
