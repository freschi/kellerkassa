package at.resch.kellerapp.model;


import at.femoweb.data.Field;
import at.femoweb.data.ForeignKey;
import at.femoweb.data.PrimaryKey;
import at.femoweb.data.Table;

/**
 * Created by felix on 8/4/14.
 */
@Table("c_cards")
public class Card {

    @Field("c_id")
    @PrimaryKey
    //@Id
    private String id;

    @Field("c_ct_type")
    @ForeignKey(field = "ct_id", mappedBy = CardType.class)
    private int type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id='" + id + '\'' +
                ", type=" + type +
                '}';
    }
}
