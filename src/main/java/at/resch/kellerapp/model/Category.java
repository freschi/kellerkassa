package at.resch.kellerapp.model;

import at.femoweb.data.Field;
import at.femoweb.data.PrimaryKey;
import at.femoweb.data.Table;

/**
 * Created by felix on 8/4/14.
 */
@Table("ca_categories")
public class Category {

    @Field("ca_id")
    @PrimaryKey
    private int id;

    @Field("ca_name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
