package at.resch.kellerapp.model;

import at.femoweb.data.Field;
import at.femoweb.data.ForeignKey;
import at.femoweb.data.PrimaryKey;
import at.femoweb.data.Table;

/**
 * Created by felix on 8/4/14.
 */
@Table("i_identities")
public class Identity {

    @Field("i_c_card")
    @PrimaryKey
    @ForeignKey(field = "c_id", mappedBy = Card.class)
    private String card;

    @Field("i_u_user")
    @PrimaryKey
    @ForeignKey(field = "u_id", mappedBy = User.class)
    private int user;

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }
}
