package at.resch.kellerapp.model;

import at.femoweb.data.Field;
import at.femoweb.data.ForeignKey;
import at.femoweb.data.PrimaryKey;
import at.femoweb.data.Table;

/**
 * Created by felix on 8/8/14.
 */
@Table("m_mixes")
public class Mix {

    @PrimaryKey
    @Field("m_ig_ingredient")
    @ForeignKey(field = "ig_id", mappedBy = Ingredient.class)
    private int ingredient;

    @PrimaryKey
    @Field("m_d_drink")
    @ForeignKey(field = "d_id", mappedBy = Drink.class)
    private int drink;

    @Field("m_amount")
    private double amount;

    public int getIngredient() {
        return ingredient;
    }

    public void setIngredient(int ingredient) {
        this.ingredient = ingredient;
    }

    public int getDrink() {
        return drink;
    }

    public void setDrink(int drink) {
        this.drink = drink;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
