package at.resch.kellerapp.model;

import at.femoweb.data.Field;
import at.femoweb.data.ForeignKey;
import at.femoweb.data.PrimaryKey;
import at.femoweb.data.Table;

/**
 * Created by felix on 8/8/14.
 */
@Table("pu_purchase_units")
public class PurchaseUnit {

    @PrimaryKey
    @Field("pu_id")
    private int id;

    @Field("pu_ig_ingredient")
    @ForeignKey(field = "ig_id", mappedBy = Ingredient.class)
    private int ingredient;

    @Field("pu_amount")
    private double amount;

    @Field("pu_barcode")
    private String barcode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIngredient() {
        return ingredient;
    }

    public void setIngredient(int ingredient) {
        this.ingredient = ingredient;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
