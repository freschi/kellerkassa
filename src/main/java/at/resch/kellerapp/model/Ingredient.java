package at.resch.kellerapp.model;


import at.femoweb.data.Field;
import at.femoweb.data.PrimaryKey;
import at.femoweb.data.Table;

/**
 * Created by felix on 8/8/14.
 */
@Table("ig_ingredients")
public class Ingredient {

    @PrimaryKey
    @Field("ig_id")
    private int id;

    @Field("ig_name")
    private String name;

    @Field("ig_unit_size")
    private double unitSize;

    @Field("ig_stock_amount")
    private double stockAmount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitSize() {
        return unitSize;
    }

    public void setUnitSize(double unitSize) {
        this.unitSize = unitSize;
    }

    public double getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(double stockAmount) {
        this.stockAmount = stockAmount;
    }
}
