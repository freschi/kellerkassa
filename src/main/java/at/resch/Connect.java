package at.resch;

import at.femoweb.Config;
import at.femoweb.data.Data;
import at.resch.kellerapp.model.Model;
import at.resch.kellerapp.model.ModelDefiner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by felix on 12/23/14.
 */
public class Connect {
    private JTextField username;
    private JPanel panel1;
    private JPasswordField password;
    private JButton connectButton;
    private JTextField url;
    
    private Logger logger = LogManager.getLogger(MainFrame.class);

    public Connect() {
        final JFrame frame = new JFrame("Hauptbuch");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(500, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        username.requestFocus();
        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Properties properties = Config.lookup(Config.FetchType.WRITEABLE, "db.conf");
                properties.setProperty("jdbc.user", username.getText());
                properties.setProperty("jdbc.url", url.getText());
                new LoadingScreen();
                try {
                    Data.init(url.getText(), username.getText(), new String(password.getPassword()), "db-con");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                frame.dispose();
                try {
                    Data.connect("db-con", ModelDefiner.class);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                try {
                    Data.injectData(Model.instance, "db-con");
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                }
            }
        });
        if(Config.has("db.conf")) {
            Properties properties = Config.lookup(Config.FetchType.READ_ONLY, "db.conf");
            if(properties.containsKey("jdbc.user")) {
                username.setText(properties.getProperty("jdbc.user"));
            }
            if(properties.containsKey("jdbc.url")) {
                url.setText(properties.getProperty("jdbc.url"));
            }
        } else {
            Config.create("db.conf");
        }
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent windowEvent) {

            }

            @Override
            public void windowClosing(WindowEvent windowEvent) {
                Config.store();
            }

            @Override
            public void windowClosed(WindowEvent windowEvent) {
                Config.store();
            }

            @Override
            public void windowIconified(WindowEvent windowEvent) {

            }

            @Override
            public void windowDeiconified(WindowEvent windowEvent) {

            }

            @Override
            public void windowActivated(WindowEvent windowEvent) {

            }

            @Override
            public void windowDeactivated(WindowEvent windowEvent) {

            }
        });
    }
}
