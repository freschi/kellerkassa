package at.resch;

import javax.swing.*;

/**
 * Created by felix on 12/23/14.
 */
public class LoadingScreen {
    private JProgressBar progressBar1;
    private JPanel panel1;
    
    public LoadingScreen () {
        JFrame frame = new JFrame("Loading...");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setSize(300, 100);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        new LoadingAnimationThread(progressBar1, frame).start();
    }
    
    public class LoadingAnimationThread extends Thread {
        
        private JProgressBar progressBar;
        private JFrame frame;

        public LoadingAnimationThread(JProgressBar progressBar, JFrame frame) {
            this.progressBar = progressBar;
            this.frame = frame;
        }

        public void run () {
            for(int i = progressBar.getMinimum(); i <= progressBar.getMaximum(); i++) {
                progressBar.setValue(i);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            frame.dispose();
            new MainFrame();
        }
    }
}
