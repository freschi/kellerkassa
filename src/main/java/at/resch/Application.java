package at.resch;

import at.femoweb.Config;
import at.resch.kellerapp.model.Model;

import javax.swing.*;
import java.util.Properties;

/**
 * Created by felix on 12/23/14.
 */
public class Application {
    
    public static void main(String[] args) {
        Config.init();
        Config.load();
        if(!Config.has("interface.conf")) {
            Config.create("interface.conf");
        }
        Properties properties = Config.lookup(Config.FetchType.WRITEABLE, "interface.conf");
        if(!properties.containsKey("startup.lookandfeel.class")) {
            properties.setProperty("startup.lookandfeel.class", "com.bulenkov.darcula.DarculaLaf");
        }
        try {
            UIManager.setLookAndFeel(properties.getProperty("startup.lookandfeel.class"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        Model.instance = new Model();
        new Connect();
    }
}
